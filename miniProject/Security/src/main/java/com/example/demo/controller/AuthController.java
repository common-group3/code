package com.example.demo.controller;

//import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.config.UserAuthProvider;
import com.example.demo.dto.CredentialDto;
import com.example.demo.dto.Signupdto;
import com.example.demo.dto.userdto;
import com.example.demo.service.UserService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor

public class AuthController {
	
	@Autowired
	UserService userservice;
	
	@Autowired
    UserAuthProvider userauthpro;
	
	@PostMapping("/login")
	public ResponseEntity <userdto> login(@RequestBody CredentialDto credentialdto){
		userdto user =userservice.login(credentialdto);
		user.setToken(userauthpro.createToken(user));
		return ResponseEntity.ok(user);
	}
	
	@PostMapping("/register")
	public ResponseEntity <userdto> register(@RequestBody Signupdto signupdto){
		userdto user=userservice.register(signupdto);
		user.setToken(userauthpro.createToken(user));
		return ResponseEntity.ok(user); //created(URI.create("/users/"+user.getId())).body(user);
	}
	
}
