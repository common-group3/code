package com.example.demo.config;

import java.util.Base64;
import java.util.Collections;
import java.util.Date;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.example.demo.dto.userdto;

import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
@Component

public class UserAuthProvider {
	
	
	private  String secretkey="san";
	
	@PostConstruct
	protected void init() {
		secretkey=Base64.getEncoder().encodeToString(secretkey.getBytes());
	}
	
	public String createToken(userdto dto) {
		Date now =new Date();
		Date Validity =new Date(now.getTime()+3_600_000);
		
		return JWT.create()
				.withIssuer(dto.getLogin())
				.withIssuedAt(now)
				.withExpiresAt(Validity)
				.withClaim("firstname",dto.getFirstname())
				.withClaim("lastname",dto.getLastname())
				.sign(Algorithm.HMAC256(secretkey));
	}
	
	public Authentication validateToken(String token) {
		
		Algorithm algorithm=Algorithm.HMAC256(secretkey);
		JWTVerifier verifier =JWT.require(algorithm).build();
		DecodedJWT decoded=verifier.verify(token);
		
		userdto user= new userdto();
		
		user.setLogin(decoded.getIssuer());
		user.setFirstname(decoded.getClaim("firstname").asString());
		user.setLastname(decoded.getClaim("lastname").asString());
		
		return new UsernamePasswordAuthenticationToken(user,null,Collections.emptyList());			
	}

}
