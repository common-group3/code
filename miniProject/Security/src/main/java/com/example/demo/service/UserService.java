package com.example.demo.service;

import java.nio.CharBuffer;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.Optional;

import com.example.demo.dto.CredentialDto;
import com.example.demo.dto.Signupdto;
import com.example.demo.dto.userdto;
import com.example.demo.entities.User;
import com.example.demo.exception.AppException;
import com.example.demo.mappers.UserMapper;
import com.example.demo.repositories.UserRepositories;

// import lombok.RequiredArgsConstructor;


@Service
public class UserService {
	
	 @Autowired
	  UserRepositories userrepo;
	 @Autowired
	 PasswordEncoder passwordencode;
	 
	 @Autowired
	 UserMapper usermap;
	 
	
	
	public userdto login(CredentialDto credentialdto) throws AppException {
		User user= userrepo.findBylogin(credentialdto.login()).orElseThrow(() -> new AppException
				("UNKOWNUSER",HttpStatus.NOT_FOUND));
		if(passwordencode.matches(CharBuffer.wrap(credentialdto.password()),(user.getPassword()))){
			return usermap.touserdto(user);
		}
		throw new AppException("INVALID PASSWORD",HttpStatus.BAD_REQUEST);
	}
	
	public userdto register(Signupdto signupdto) {
		Optional<User> ouser=userrepo.findBylogin(signupdto.getLogin());
		if(ouser.isPresent()) {
			throw new AppException ("Login already Exists",HttpStatus.BAD_REQUEST);
		}
		
		//Signupdto suser= (usermap.signuptouser(signupdto));
		User user = new User();
		user.setFirstname(signupdto.getFirstname());
		user.setLastname(signupdto.getLastname());
		user.setLogin(signupdto.getLogin());
		user.setPassword(passwordencode.encode(CharBuffer.wrap(signupdto.getPassword())));
		User saveduser=userrepo.save(user);
		
		return usermap.touserdto(saveduser);
	}
}
