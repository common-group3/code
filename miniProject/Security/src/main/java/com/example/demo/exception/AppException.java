package com.example.demo.exception;

import org.springframework.http.HttpStatus;

public class AppException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final HttpStatus httpstatus ;
	
	public AppException(String message,HttpStatus httpstatus) {
		super(message);
		this.httpstatus=httpstatus;
	}
	
	public HttpStatus getHttpStatus() {
		return httpstatus;
	}
	

}
