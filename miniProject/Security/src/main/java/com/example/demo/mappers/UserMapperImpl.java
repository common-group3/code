package com.example.demo.mappers;

import org.springframework.stereotype.Component;

import com.example.demo.dto.Signupdto;
import com.example.demo.dto.userdto;
import com.example.demo.entities.User;

@Component
public class UserMapperImpl implements UserMapper{

	@Override
	public userdto touserdto(User user) {
		userdto userDTO = new userdto();
		userDTO.setFirstname(user.getFirstname());
		userDTO.setLastname(user.getLastname());
		userDTO.setLogin(user.getLogin());
		userDTO.setPassword(user.getPassword());
		
		return userDTO;
	}

	@Override
	public Signupdto signuptouser(Signupdto signupdto) {
		Signupdto signdto =new Signupdto();
		
		signdto.setFirstname(signupdto.getFirstname());
		signdto.setLastname(signupdto.getLastname());
		signdto.setLogin(signupdto.getLogin());
		signdto.setPassword(signupdto.getPassword());
		
		return signdto;
	}
	

}
