package com.example.demo.model;

import jakarta.persistence.*;
@Entity
@Table (name="registeration")
public class Registeration {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column (name="student_id")
	private int id;

	@Column (name="student_username")
	private String username;
	@Column (name="student_fathername")
	private String fathername;
	@Column (name="student_mothername")
	private String mothername;
	@Column (name="student_password")
	private String password;
	@Column (name="student_address")
	private String address;
	@Column (name="student_pincode")
	private Integer userid;
	
	public Registeration(String username,String fathername,String mothername,String password,String address,Integer pincode, Integer userid) {
		this.username=username;
		this.fathername=fathername;
		this.mothername=mothername;
		this.password=password;
		this.address=address;
		this.userid=userid;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
		
		public Integer getUserid() {
		return userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}

		public String getUsername() {
			return username;
		}
		public void setUsername(String username) {
			this.username = username;
		}
		public String getFathername() {
			return fathername;
		}
		public void setFathername(String fathername) {
			this.fathername = fathername;
		}
		public String getMothername() {
			return mothername;
		}
		public void setMothername(String mothername) {
			this.mothername = mothername;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}

		public Registeration(int id, String username, String fathername, String mothername, String password,
				String address, Integer userid) {
			super();
			this.id = id;
			this.username = username;
			this.fathername = fathername;
			this.mothername = mothername;
			this.password = password;
			this.address = address;
			this.userid = userid;
		}

		public Registeration() {
			super();
		}
		
	
}

