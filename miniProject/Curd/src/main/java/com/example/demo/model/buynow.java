package com.example.demo.model;

import jakarta.persistence.*;

@Entity
@Table (name="buynow_details")
public class buynow {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getProductprice() {
		return productprice;
	}
	public void setProductprice(Integer productprice) {
		this.productprice = productprice;
	}
	public String getProductname() {
		return productname;
	}
	public void setProductname(String productname) {
		this.productname = productname;
	}
	public String getProductrole() {
		return productrole;
	}
	public void setProductrole(String productrole) {
		this.productrole = productrole;
	}
	public String getProductcolour() {
		return productcolour;
	}
	public void setProductcolour(String productcolour) {
		this.productcolour = productcolour;
	}
	public String getProductdescription() {
		return productdescription;
	}
	public void setProductdescription(String productdescription) {
		this.productdescription = productdescription;
	}
	public String getProductimageurl() {
		return productimageurl;
	}
	public void setProductimageurl(String productimageurl) {
		this.productimageurl = productimageurl;
	}
	public Integer getProductquantity() {
		return productquantity;
	}
	public void setProductquantity(Integer productquantity) {
		this.productquantity = productquantity;
	}
	@Column(name="product_price")
	private Integer productprice;
	@Column(name="product_name")
	private String productname;
	@Column(name="product_role")
	private String productrole;
	@Column(name="product_colour")
	private String productcolour;
	@Column(name="product_description")
	private String productdescription;
	@Column(name="product_imageurl")
	private String productimageurl;
	@Column(name="quantity")
	private Integer productquantity;
	
}
