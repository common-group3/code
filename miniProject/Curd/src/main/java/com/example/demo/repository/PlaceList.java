package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.placelist;

public interface PlaceList extends JpaRepository <placelist,Integer>{
	 placelist findByid(Integer id);
}
