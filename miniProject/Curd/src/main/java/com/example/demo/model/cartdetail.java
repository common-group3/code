package com.example.demo.model;

import jakarta.persistence.*;
@Entity
@Table (name="cartdetail")
public class cartdetail {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column (name="price")
	private Integer productprice;
	public Integer getProductprice() {
		return productprice;
	}
	public void setProductprice(Integer productprice) {
		this.productprice = productprice;
	}
	@Column (name="name")
	private String productname;
	@Column (name="role")
	private String productrole;
	@Column (name="colour")
	private String productcolour;
	@Column(name="description")
	private String productdescription;
	@Column(name="imageurl")
	private String productimageurl;
	@Column(name="quantity")
	private Integer productquantity;
	
	public Integer getProductquantity() {
		return productquantity;
	}
	public void setProductquantity(Integer productquantity) {
		this.productquantity = productquantity;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getProductname() {
		return productname;
	}
	public void setProductname(String productname) {
		this.productname = productname;
	}
	public String getProductrole() {
		return productrole;
	}
	public void setProductrole(String productrole) {
		this.productrole = productrole;
	}
	public String getProductcolour() {
		return productcolour;
	}
	public void setProductcolour(String productcolour) {
		this.productcolour = productcolour;
	}
	public String getProductdescription() {
		return productdescription;
	}
	public void setProductdescription(String productdescription) {
		this.productdescription = productdescription;
	}
	public String getProductimageurl() {
		return productimageurl;
	}
	public void setProductimageurl(String productimageurl) {
		this.productimageurl = productimageurl;
	}
	
}
