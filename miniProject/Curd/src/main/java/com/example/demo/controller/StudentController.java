package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Registeration;
import com.example.demo.model.addproducts;
import com.example.demo.model.addproductvo;
import com.example.demo.model.addproductvoo;
import com.example.demo.model.buynow;
import com.example.demo.model.buynowvo;
import com.example.demo.model.cartdetail;
import com.example.demo.model.cartproductvo;
import com.example.demo.model.loginVo;
import com.example.demo.model.placelist;
import com.example.demo.model.placelistvo;
import com.example.demo.model.responce;
import com.example.demo.model.responce1;
import com.example.demo.model.populate;
import com.example.demo.model.registerationvo;
import com.example.demo.service.StudentService;


@CrossOrigin("http://localhost:4200")
@RestController
@RequestMapping("/student")
public class StudentController {
	
	@Autowired
	StudentService studentservice;
	
	
	@PostMapping("/registeration")
	public Registeration adddata(@RequestBody Registeration regi) {
		return this.studentservice.addStudentService(regi);
	}
	
	@PostMapping("/buynow")
	public buynow buydata(@RequestBody buynow brgi) {
		return this.studentservice.buydata2(brgi);
	}
	@PostMapping("/login")
	public responce loginuser1(@RequestBody loginVo regi){
		 return this.studentservice.loginuser2(regi);
	}
	@PostMapping ("/selleradd")
	public addproducts adddata(@RequestBody addproducts argi) {
		return studentservice.adddata2(argi);
	}
	@PostMapping ("/addcart")
	public cartdetail putdata (@RequestBody cartdetail crgi) {
		return studentservice.putdata2(crgi);
	}
	@PostMapping ("/orderlist")
	public placelist placedata(@RequestBody placelist prgi) {
		return studentservice.placedata2(prgi);
	}
	@GetMapping ("/orderget")
	public List<placelistvo> getall(){
		return studentservice.getall2();
	}
	@GetMapping("/buyall")
	public List<buynowvo> buyall(){
		return studentservice.buyall2();
	}
	@GetMapping ("/selleradd")
	public List<addproductvo> getdata(){
		return studentservice.getAllProductData();
	}
	@GetMapping ("/addcart")
	public List <cartproductvo> cartdata(){
		return studentservice.getallcart();
	}
	
	@GetMapping ("/deleteall")
	public responce deleteall() {
		return studentservice.deleteall2();
	}
	@GetMapping("/deletenow")
	public responce deletenow() {
		return studentservice.deletenow2();
	}
	
	@GetMapping("/home")
	public List <addproductvoo> populate(){
		return studentservice.getAllproducthome();
	}
	
	@GetMapping ("/sellerdelete")
	public responce1 deletedata(@RequestParam (value="id") Integer id) {
		return studentservice.deletedata2(id);
	}
	@GetMapping ("/orderdelete")
	public responce orderdelete(@RequestParam (value="id") Integer id) {
		return studentservice.orderdelete2(id);
	}
	@GetMapping ("/cartdelete")
	public responce1 deletecart(@RequestParam (value="id") Integer id) {
		return studentservice.deletecart2(id);
	}
	@GetMapping ("/record")
	public populate  getdatas(@RequestParam (value="id") Integer id) {
		return studentservice.getdatas2(id);
	}
	@GetMapping ("/record1")
	public populate getdata(@RequestParam (value="proname") String proname) {
		return studentservice.getdata2(proname);
	}
	@GetMapping ("/getunique")
	public populate getunidata (@RequestParam (value="id") Integer id) {
		return studentservice.getunidata2(id);
	}
	@GetMapping ("/useredit")
	public registerationvo getedit(@RequestParam (value="str") String str) {
		return studentservice.getedit2(str);
	}
	
	@PutMapping("/editdone/{id}")
	public ResponseEntity<Registeration> ubdate(@PathVariable("id") Integer id, @RequestBody Registeration urgi) {
	    Registeration updatedRecord = studentservice.update2(id, urgi);
	    
	    if (updatedRecord != null) {
	        return ResponseEntity.ok(updatedRecord); // Return 200 OK with the updated record
	    } else {
	        return ResponseEntity.notFound().build(); // Return 404 Not Found if the record is not found
	    }
	}
	
}
