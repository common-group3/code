package com.example.demo.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.addproducts;

public interface AddProduct extends JpaRepository <addproducts,Integer> {
	 addproducts findByproductname(String username);
	 addproducts findByid(Integer id);
	
}
