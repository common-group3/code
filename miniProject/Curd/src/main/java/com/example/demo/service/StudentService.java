package com.example.demo.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.Registeration;
import com.example.demo.model.addproducts;
import com.example.demo.model.addproductvo;
import com.example.demo.model.addproductvoo;
import com.example.demo.model.buynow;
import com.example.demo.model.buynowvo;
import com.example.demo.model.cartdetail;
import com.example.demo.model.cartproductvo;
import com.example.demo.model.loginVo;
import com.example.demo.model.populate;
import com.example.demo.model.registerationvo;
import com.example.demo.model.responce;
import com.example.demo.model.responce1;
import com.example.demo.repository.AddProduct;
import com.example.demo.repository.BuyNow;
import com.example.demo.repository.CartProduct;
import com.example.demo.repository.PlaceList;
import com.example.demo.repository.StudentRegi;
import com.example.demo.model.placelist;
import com.example.demo.model.placelistvo;

@Service
public class StudentService {
	
	@Autowired
	StudentRegi studentregi;
	
	@Autowired
	AddProduct addproduct;
	
	@Autowired
	CartProduct cartproduct;
	
	@Autowired
	PlaceList placelist;
	
	@Autowired
	BuyNow buynows;
	
	public Registeration addStudentService(Registeration reg){
		return this.studentregi.save(reg);
	}
	public addproducts adddata2 (addproducts argi) {
		return this.addproduct.save(argi);
	}
	public buynow buydata2(buynow brgi) {
		return this.buynows.save(brgi);
	}
	public cartdetail putdata2(cartdetail crgi) {
		return this.cartproduct.save(crgi);
	}
	public placelist placedata2(placelist prgi) {
		return this.placelist.save(prgi);
	}
	public responce deleteall2() {
		this.cartproduct.deleteAll();
		responce responcem=new responce();
		responcem.setResponceMsg("deleted all content");
		return responcem;
	}
	public responce deletenow2() {
		this.buynows.deleteAll();
		responce responcem=new responce();
		responcem.setResponceMsg("deleted all content");
		return responcem;
	}
	
	public responce loginuser2 (loginVo regidata){
		responce responceVo = new responce();
		Registeration regi=studentregi.findByusername(regidata.getUsername());
		if(regi != null) {
		if(regi.getPassword().equals(regidata.getPassword())) {
			responceVo.setResponceMsg("LoginSuccessfully");
			return responceVo;
		}
		}	
		responceVo.setResponceMsg("PleaseRegister");
			return responceVo;
	}
	public List<addproductvo> getAllProductData() {
		List<addproducts> data = addproduct.findAll();
		List<addproductvo> AllData = new ArrayList<>();
		
		for (addproducts value : data) {
			addproductvo product = new addproductvo();
			product.setId(value.getId());
			product.setProductprice(value.getProductprice());
			product.setProductname(value.getProductname());
			product.setProductrole(value.getProductrole());
			product.setProductcolour(value.getProductcolour());
			product.setProductdescription(value.getProductdescription());
			product.setProductimageurl(value.getProductimageurl());
		AllData.add(product);	
		}
		
		return AllData;
	}
	public List<placelistvo> getall2(){
		List<placelist> data =placelist.findAll();
		List<placelistvo> AllData =new ArrayList<>();
		for(placelist value:data) {
			placelistvo order =new placelistvo();
			order.setId(value.getId());
			order.setEmail(value.getEmail());
			order.setUsername(value.getUsername());
			order.setContact(value.getContact());
			order.setAddress(value.getAddress());
			order.setAmount(value.getAmount());
		AllData.add(order);
		}
		return AllData;
	}
	public List<cartproductvo> getallcart(){
		List <cartdetail> data =cartproduct.findAll();
		List <cartproductvo> view =new ArrayList<>();
		
		for(cartdetail value:data) {
			cartproductvo product =new cartproductvo();
			product.setId(value.getId());
			product.setProductname(value.getProductname());
			product.setProductprice(value.getProductprice());
			product.setProductquantity(value.getProductquantity());
			product.setProductimageurl(value.getProductimageurl());
			view.add(product);
		}
		return view;
		
	}
	public List<addproductvoo> getAllproducthome(){
		List<addproducts> data = addproduct.findAll();
		List<addproductvoo> view =new ArrayList<>();
		
		for(addproducts value : data) {
			addproductvoo product =new addproductvoo();
			product.setId(value.getId());
			product.setProductprice(value.getProductprice());
			product.setProductname(value.getProductname());
			product.setProductrole(value.getProductrole());
			product.setProductcolour(value.getProductcolour());
			product.setProductdescription(value.getProductdescription());
			product.setProductimageurl(value.getProductimageurl());
			view.add(product);
		}
		return view;
		}
	public List <buynowvo> buyall2(){
		List<buynow> data = buynows.findAll();
		List <buynowvo> view =new ArrayList<>();
		
		for(buynow value:data) {
			buynowvo product =new buynowvo();
			product.setId(value.getId());
			product.setProductprice(value.getProductprice());
			product.setProductname(value.getProductname());
			product.setProductrole(value.getProductrole());
			product.setProductcolour(value.getProductcolour());
			product.setProductdescription(value.getProductdescription());
			product.setProductimageurl(value.getProductimageurl());
			product.setProductquantity(value.getProductquantity());
			view.add(product);
		}
		if (view.isEmpty()) {
			 return Collections.emptyList(); // Return an empty list
		}
		return view;
	}
		
	public responce1 deletedata2 (Integer id) {
		this.addproduct.deleteById(id);
		responce1 responceVo1 = new responce1();
		responceVo1.setResponceMsg1("Successfully Deleted");
		return responceVo1;
	}
	public responce orderdelete2(Integer id) {
		this.placelist.deleteById(id);
		responce responcem =new responce();
		responcem.setResponceMsg("Deleted");
		return responcem;
	}
	public responce1 deletecart2 (Integer id) {
		this.cartproduct.deleteById(id);
		responce1 responcevo1 =new responce1();
		responcevo1.setResponceMsg1("deleted");
		return responcevo1;
	} 
	
	public populate getdatas2(Integer id) {
		populate populatevo= new populate();
		addproducts argi =addproduct.findByid(id);
		populatevo.setId(argi.getId());
		populatevo.setProductprice(argi.getProductprice());
		populatevo.setProductname(argi.getProductname());
		populatevo.setProductrole(argi.getProductrole());
		populatevo.setProductcolour(argi.getProductcolour());
		populatevo.setProductdescription(argi.getProductdescription());
		populatevo.setProductimageurl(argi.getProductimageurl());
		return populatevo;
	}
	public populate getdata2(String proname) {
	    populate populatevo = new populate();
	    addproducts argi = addproduct.findByproductname(proname);
	    
	    // Check if the product was found by the product name
	    if (argi == null) {
	        // Product not found, return null
	        return null;
	    }

	    populatevo.setId(argi.getId());
	    populatevo.setProductprice(argi.getProductprice());
	    populatevo.setProductname(argi.getProductname());
	    populatevo.setProductrole(argi.getProductrole());
	    populatevo.setProductcolour(argi.getProductcolour());
	    populatevo.setProductdescription(argi.getProductdescription());
	    populatevo.setProductimageurl(argi.getProductimageurl());

	    return populatevo;
	}
	public registerationvo getedit2(String str) {
		registerationvo regivo=new registerationvo();
		Registeration regi = studentregi.findByusername(str);
		
		if (regi == null) {
			return null;
		}
		regivo.setId(regi.getId());
		regivo.setUsername(regi.getUsername());
		regivo.setUserid(regi.getUserid());
		regivo.setPassword(regi.getPassword());
		regivo.setAddress(regi.getAddress());
		regivo.setFathername(regi.getFathername());
		regivo.setMothername(regi.getMothername());
		
		return regivo;
	}

	public populate getunidata2(Integer id) {		
		populate responcevo=new populate();
		addproducts argi=addproduct.findByid(id);
		responcevo.setId(argi.getId());
		responcevo.setProductprice(argi.getProductprice());
		responcevo.setProductname(argi.getProductname());
		responcevo.setProductrole(argi.getProductrole());
		responcevo.setProductcolour(argi.getProductcolour());
		responcevo.setProductdescription(argi.getProductdescription());
		responcevo.setProductimageurl(argi.getProductimageurl());
		return responcevo;
	}
	
	public Registeration update2(Integer id,Registeration regi) {
		Registeration info=studentregi.findByid(id);
		info.setId(regi.getId());
		info.setUserid(regi.getUserid());
		info.setUsername(regi.getUsername());
		info.setAddress(regi.getAddress());
		info.setPassword(regi.getPassword());
		info.setFathername(regi.getFathername());
		info.setMothername(regi.getMothername());
		
		return studentregi.save(info);
	}
	
}
