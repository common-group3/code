package com.example.demo.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;


import com.example.demo.model.cartdetail;

public interface CartProduct extends JpaRepository <cartdetail,Integer>{
	  cartdetail findByproductname(String username);
	  Optional<cartdetail> findByid(Integer id);
}
