package com.example.demo.repository;



import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Registeration;

public interface StudentRegi extends JpaRepository<Registeration, Integer>{
	
	    Registeration findByid(Integer Id);
	    Registeration findByusername(String username);

}
